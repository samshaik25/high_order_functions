function reduce(array,cb,startingValue)
{
  if(!Array.isArray(array) ||  typeof cb !== 'function')
  {
    throw new Error("invalid arguments passed")

  }
  let i=0;
  if(typeof startingValue ==='undefined')
  {
    startingValue=array[0];
    i=1;
  }
  let accumulator =startingValue;

   for(;i<array.length;i++)
   {
     accumulator=cb(accumulator,array[i],i,array)

   }
 return accumulator;

}
module.exports.reduce=reduce;
