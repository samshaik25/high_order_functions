 function each (array,cb)
{
    if(!Array.isArray(array)  ||  typeof cb!=='function')
    {
        throw new Error("invalid argument passed")
    }
    for(let i=0;i<array.length;i++)
    {
        cb(array[i],i)
    }
}
module.exports.each = each