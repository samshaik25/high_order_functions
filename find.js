
function find(array,cb)
{  
    if(!Array.isArray(array)  || typeof cb!=='function')
    {
        throw new Error("invalid argument")
    }


    let newA=[];
    let isFound=false;
    for(let i=0;i<array.length;i++)
    {
        if(cb(array[i],i,array))
        {
            console.log(array[i])
            isFound=true;
        }
    }
    if(isFound===false)
    {
    
        console.log('undefined');
    }
}

module.exports.find=find;