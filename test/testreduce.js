const {reduce} = require('../reduce.js')

let items = [1,2,3,4,5];
let result;
try{
   result= reduce(items,(prevValue,currValue)=>
    {
       return prevValue+currValue;
    })
    console.log(result)
} catch(e){
    console.log(e.message)
}

