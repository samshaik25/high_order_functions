function  filter(array,cb)
{

	if(!Array.isArray(array) || typeof cb !=='function')
	{
		throw new Error("invalid arguments")
	}
	newArray=[];

	for(let i=0;i<array.length;i++)
		{
		   if(cb(array[i],i))
		   {
		   newArray.push(array[i]);
		   }
		}
		return newArray;
}

module.exports.filter=filter;

