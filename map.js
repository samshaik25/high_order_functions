function map(array,cb)
{  
     if(!Array.isArray(array)  ||  typeof cb !== 'function')
	 {
		 throw new Error("invalid argument");
	 }

	let newArray=[];
	for(let i=0;i<array.length;i++)
	{
		newArray.push(cb(array[i],i,array))
	}
	return newArray;
}

module.exports.map=map; 
