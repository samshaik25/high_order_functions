  

function flatten(array)
{

  if(!Array.isArray(array))
  {
	  throw new Error("Invalid argument ") ;
  }

  let flatArray=[];

	 function recur(array)
	{
		for(let i=0;i<array.length;i++)
		{
			let element=array[i];
                       
			if(Array.isArray(element))
			{
				recur(element);
			}
			else 
			{
				flatArray.push(element);
			}
		}
	}
     recur(array);

	return flatArray;
}

module.exports.flatten=flatten;

